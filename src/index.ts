import MovieService from './movieService';
import { imgUrl } from './constants';
import Movie from './movie';

/////____________________________Var___________________________________________
let popularMoviesCurrentPage = 1;
let upcomingMoviesCurrentPage = 1;
let topRatedMoviesCurrentPage = 1;
let searchedMoviesCurrentPage = 1;
let btnGroupIsSelected = 'popular';
// let favouriteMoviesArray: number[] = [520763, 581726, 337404];

export async function render(): Promise<void> {
    // TODO render your app here
    let favouriteMoviesArray =
        JSON.parse(localStorage.getItem('favouriteMovies') || '[]') || [];

    const onAddFavouriteMovie = function (movieId: number) {
        favouriteMoviesArray.push(movieId);
        localStorage.setItem(
            'favouriteMovies',
            JSON.stringify(favouriteMoviesArray)
        );
        // renderFavouriteMoviesList(favouriteMoviesArray);
    };
    const onRemoveFavouriteMovie = function (movieId: number): void {
        favouriteMoviesArray = favouriteMoviesArray.filter(
            (id: number) => id !== movieId
        );
        localStorage.setItem(
            'favouriteMovies',
            JSON.stringify(favouriteMoviesArray)
        );
        // renderFavouriteMoviesList(favouriteMoviesArray);
    };

    //////////____________________DOM_Elems_______________________________________________________
    const searchButton: HTMLElement | null = document.getElementById('submit');
    const searchInput: HTMLElement | null = document.getElementById('search');
    const filmContainer: HTMLElement | null =
        document.getElementById('film-container');
    const popularRadioBtn: HTMLElement | null =
        document.getElementById('popular');
    const upcomingRadioBtn: HTMLElement | null =
        document.getElementById('upcoming');
    const topRatedRadioBtn: HTMLElement | null =
        document.getElementById('top_rated');
    const btnGroup: NodeList | null = document.querySelectorAll(
        '#button-wrapper>input'
    );
    const loadMoreBtn: HTMLElement | null =
        document.getElementById('load-more');
    const randomMovieName: HTMLElement | null =
        document.getElementById('random-movie-name');
    const randomMovieDescription: HTMLElement | null = document.getElementById(
        'random-movie-description'
    );
    const randomMovieContainer: HTMLElement | null =
        document.getElementById('random-movie');
    const favouriteMoviesContainer: HTMLElement | null =
        document.getElementById('favorite-movies');
    const favoritesModal: HTMLElement | null =
        document.getElementById('favoritesModal');
    const closeModal: HTMLElement | null =
        document.getElementById('closeModal');
    // const checkBoxLikeButtons: NodeList | null =
    //     document.querySelectorAll('.checkBoxLike');
    // console.log(checkBoxLikeButtons);
    ////////_____________________onClick_Functions___________________________________________________

    const getMoviesByName = async function (searchedMoviesCurrentPage: number) {
        if (
            searchInput &&
            (searchInput as HTMLInputElement).value !== '' &&
            (searchInput as HTMLInputElement).value !== null
        ) {
            const result = await apiClient.getMoviesByName(
                (searchInput as HTMLInputElement).value,
                searchedMoviesCurrentPage
            );
            Array.from(btnGroup).forEach((element: Node) => {
                (element as HTMLInputElement).checked = false;
            });
            return renderFilmContainer(result);
        }
    };

    const getPopularMovies = async function (popularMoviesCurrentPage: number) {
        const result = await apiClient.getPopularMovies(
            popularMoviesCurrentPage
        );
        renderRandomMovieSection(result);
        return renderFilmContainer(result);
    };

    const getUpcomingMovies = async function (
        upcomingMoviesCurrentPage: number
    ) {
        const result = await apiClient.getUpcomingMovies(
            upcomingMoviesCurrentPage
        );
        renderRandomMovieSection(result);
        return renderFilmContainer(result);
    };
    const getTopRatedMovies = async function (
        topRatedMoviesCurrentPage: number
    ) {
        const result = await apiClient.getTopRatedMovies(
            topRatedMoviesCurrentPage
        );
        renderRandomMovieSection(result);
        return renderFilmContainer(result);
    };

    const loadMoreMovies = async function () {
        console.log(btnGroupIsSelected);
        switch (btnGroupIsSelected) {
            case 'popular':
                popularMoviesCurrentPage++;
                await getPopularMovies(popularMoviesCurrentPage);
                break;
            case 'upcoming':
                upcomingMoviesCurrentPage++;
                await getUpcomingMovies(upcomingMoviesCurrentPage);
                break;
            case 'topRated':
                topRatedMoviesCurrentPage++;
                await getTopRatedMovies(topRatedMoviesCurrentPage);
                break;
            case 'search':
                searchedMoviesCurrentPage++;
                await getMoviesByName(searchedMoviesCurrentPage);
                break;
        }
    };

    ///___________________________Event_Handlers__________________________________________________________
    if (searchButton)
        searchButton.onclick = function () {
            upcomingMoviesCurrentPage = 1;
            topRatedMoviesCurrentPage = 1;
            popularMoviesCurrentPage = 1;
            btnGroupIsSelected = 'search';
            return getMoviesByName(searchedMoviesCurrentPage);
        };

    if (popularRadioBtn)
        popularRadioBtn.onchange = function () {
            upcomingMoviesCurrentPage = 1;
            topRatedMoviesCurrentPage = 1;
            searchedMoviesCurrentPage = 1;
            (searchInput as HTMLInputElement).value = '';
            btnGroupIsSelected = 'popular';
            return getPopularMovies(popularMoviesCurrentPage);
        };

    if (upcomingRadioBtn)
        upcomingRadioBtn.onchange = function () {
            popularMoviesCurrentPage = 1;
            topRatedMoviesCurrentPage = 1;
            searchedMoviesCurrentPage = 1;
            (searchInput as HTMLInputElement).value = '';
            btnGroupIsSelected = 'upcoming';
            getUpcomingMovies(upcomingMoviesCurrentPage);
        };

    if (topRatedRadioBtn)
        topRatedRadioBtn.onchange = function () {
            popularMoviesCurrentPage = 1;
            upcomingMoviesCurrentPage = 1;
            searchedMoviesCurrentPage = 1;
            (searchInput as HTMLInputElement).value = '';
            btnGroupIsSelected = 'topRated';
            getTopRatedMovies(topRatedMoviesCurrentPage);
        };

    if (loadMoreBtn) loadMoreBtn.onclick = () => loadMoreMovies();

    if (favoritesModal)
        favoritesModal.onclick = () =>
            renderFavouriteMoviesList(favouriteMoviesArray);

    if (closeModal)
        closeModal.onclick = () => getPopularMovies(popularMoviesCurrentPage);
    /////___________________________renders_______________________________________________________

    function renderFavouriteMoviesList(favouriteMoviesArray: number[]) {
        if (favouriteMoviesContainer) favouriteMoviesContainer.innerHTML = '';
        favouriteMoviesArray.forEach(async function (movieId) {
            const movie = await apiClient.getMovieById(movieId);

            const divCardShadow: HTMLElement = document.createElement('div');
            const imgPoster: HTMLImageElement = document.createElement('img');
            const labelForLikeCheckBox: HTMLElement =
                document.createElement('label');
            document.createElement('div');
            const checkBoxIsLiked: HTMLInputElement =
                document.createElement('input');
            const cardBody: HTMLElement = document.createElement('div');
            const cardText: HTMLElement = document.createElement('p');
            const releasedDataContainer: HTMLElement =
                document.createElement('div');
            const smallReleasedDate: HTMLElement =
                document.createElement('small');
            const divCardContainer: HTMLDivElement =
                document.createElement('div');
            checkBoxIsLiked.id = movie.id;
            if (favouriteMoviesArray.includes(movie.id))
                checkBoxIsLiked.checked = true;
            checkBoxIsLiked.classList.add('checkBoxLike');
            labelForLikeCheckBox.setAttribute('for', 'checkBoxIsLiked');
            smallReleasedDate.classList.add('text-muted');
            smallReleasedDate.innerText = movie.releaseDate;
            releasedDataContainer.classList.add(
                'd-flex',
                'justify-content-between',
                'align-items-center'
            );
            cardText.classList.add('card-text', 'truncate');
            cardText.innerText = movie.overview;
            cardBody.classList.add('card-body');
            checkBoxIsLiked.setAttribute('type', 'checkbox');
            checkBoxIsLiked.addEventListener('change', function (event) {
                const target = event.target;
                if ((target as HTMLInputElement).checked === true) {
                    onAddFavouriteMovie(
                        Number((target as HTMLInputElement).id)
                    );
                    console.log(favouriteMoviesArray);
                } else {
                    onRemoveFavouriteMovie(
                        Number((target as HTMLInputElement).id)
                    );

                    console.log(favouriteMoviesArray);
                }
            });
            labelForLikeCheckBox.classList.add(
                'bi',
                'bi-heart-fill',
                'position-absolute',
                'p-2',
                'labelForLikeCheckBox'
            );
            imgPoster.src = imgUrl + movie.posterPath;
            divCardShadow.classList.add('card', 'shadow-sm');
            labelForLikeCheckBox.setAttribute('for', `${movie.id}`);
            labelForLikeCheckBox.appendChild(checkBoxIsLiked);
            releasedDataContainer.appendChild(smallReleasedDate);
            cardBody.appendChild(cardText);
            cardBody.appendChild(releasedDataContainer);
            divCardShadow.appendChild(imgPoster);
            divCardShadow.appendChild(labelForLikeCheckBox);
            divCardShadow.appendChild(cardBody);
            divCardContainer.appendChild(divCardShadow);
            divCardContainer.classList.add('col-12', 'p-2');

            favouriteMoviesContainer?.appendChild(divCardContainer);
        });
    }

    function renderRandomMovieSection(movies: Movie[]): void {
        const i = Math.floor(Math.random() * movies.length);
        if (randomMovieName) randomMovieName.innerText = movies[i].title;
        if (randomMovieDescription)
            randomMovieDescription.innerText = movies[i].overview;
        if (randomMovieContainer)
            randomMovieContainer.style.backgroundImage = `url('${
                imgUrl + movies[i].posterPath
            }')`;
    }

    function renderFilmContainer(movies: Movie[]): void {
        if (filmContainer) filmContainer.innerHTML = '';
        movies.map(function (movie) {
            const divContainer: HTMLElement = document.createElement('div');
            const divCardShadow: HTMLElement = document.createElement('div');
            const imgPoster: HTMLImageElement = document.createElement('img');
            const labelForLikeCheckBox: HTMLElement =
                document.createElement('label');
            document.createElement('div');
            const checkBoxIsLiked: HTMLInputElement =
                document.createElement('input');
            const cardBody: HTMLElement = document.createElement('div');
            const cardText: HTMLElement = document.createElement('p');
            const releasedDataContainer: HTMLElement =
                document.createElement('div');
            const smallReleasedDate: HTMLElement =
                document.createElement('small');
            // const heartSVG: HTMLElement = document.createElement('i');

            checkBoxIsLiked.id = movie.id;
            if (favouriteMoviesArray.includes(movie.id))
                checkBoxIsLiked.checked = true;
            checkBoxIsLiked.classList.add('checkBoxLike');
            labelForLikeCheckBox.setAttribute('for', 'checkBoxIsLiked');
            smallReleasedDate.classList.add('text-muted');
            smallReleasedDate.innerText = movie.releaseDate;
            releasedDataContainer.classList.add(
                'd-flex',
                'justify-content-between',
                'align-items-center'
            );
            cardText.classList.add('card-text', 'truncate');
            cardText.innerText = movie.overview;
            cardBody.classList.add('card-body');
            checkBoxIsLiked.setAttribute('type', 'checkbox');
            checkBoxIsLiked.addEventListener('change', function (event) {
                const target = event.target;
                if ((target as HTMLInputElement).checked === true) {
                    onAddFavouriteMovie(
                        Number((target as HTMLInputElement).id)
                    );
                    console.log(favouriteMoviesArray);
                } else {
                    onRemoveFavouriteMovie(
                        Number((target as HTMLInputElement).id)
                    );
                    console.log(favouriteMoviesArray);
                }
            });
            labelForLikeCheckBox.classList.add(
                'bi',
                'bi-heart-fill',
                'position-absolute',
                'p-2',
                'labelForLikeCheckBox'
            );
            imgPoster.src = imgUrl + movie.posterPath;
            divCardShadow.classList.add('card', 'shadow-sm');
            divContainer.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');
            labelForLikeCheckBox.setAttribute('for', `${movie.id}`);
            labelForLikeCheckBox.appendChild(checkBoxIsLiked);
            releasedDataContainer.appendChild(smallReleasedDate);
            cardBody.appendChild(cardText);
            cardBody.appendChild(releasedDataContainer);
            divCardShadow.appendChild(imgPoster);
            divCardShadow.appendChild(labelForLikeCheckBox);
            divCardShadow.appendChild(cardBody);
            divContainer.appendChild(divCardShadow);
            filmContainer?.appendChild(divContainer);
            // setFillMovieIsLiked();
        });
    }
    const apiClient = new MovieService();
    getPopularMovies(popularMoviesCurrentPage);
    renderFavouriteMoviesList(favouriteMoviesArray);

    // localStorage.setItem('favouriteMovies', JSON.stringify(''));
    // apiClient.getMovieByName('hello').then((movies) => console.log(movies));
}
