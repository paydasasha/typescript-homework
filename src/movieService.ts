import { api_key } from './constants';
import Movie from './movie';

/////____________________________Services______________________________________-
class MovieService {
    formatMovie(movie: any): Movie {
        return {
            title: movie.title,
            id: movie.id,
            releaseDate: movie.releaseDate,
            overview: movie.overview,
            posterPath: movie.poster_path,
        };
    }
    callApi(url: string): Promise<Movie[]> {
        return fetch(url)
            .then((res) => res.json())
            .then((res) =>
                res['results'].map((movie: any) => this.formatMovie(movie))
            );
    }

    getMoviesByName(
        query: string,
        searchedMoviesCurrentPage = 1
    ): Promise<Movie[]> {
        return this.callApi(
            `https://api.themoviedb.org/3/search/movie?api_key=${api_key}&language=en-US&query=${query}&page=${searchedMoviesCurrentPage}&include_adult=false`
        );
    }
    getPopularMovies(popularMoviesCurrentPage = 1): Promise<Movie[]> {
        return this.callApi(
            `https://api.themoviedb.org/3/movie/popular?api_key=${api_key}&language=en-US&page=${popularMoviesCurrentPage}`
        );
    }
    getUpcomingMovies(upcomingMoviesCurrentPage = 1): Promise<Movie[]> {
        return this.callApi(
            `https://api.themoviedb.org/3/movie/upcoming?api_key=${api_key}&language=en-US&page=${upcomingMoviesCurrentPage}`
        );
    }
    getTopRatedMovies(topRatedMoviesCurrentPage = 1): Promise<Movie[]> {
        return this.callApi(
            `https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}&language=en-US&page=${topRatedMoviesCurrentPage}`
        );
    }
    getMovieById(id: number): Promise<Movie> {
        return fetch(
            `https://api.themoviedb.org/3/movie/${id}?api_key=${api_key}&language=en-US`
        )
            .then((res) => res.json())
            .then((res) => this.formatMovie(res));
    }
}

export default MovieService;
