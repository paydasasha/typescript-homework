//////___________Classes_________________________________________________
class Movie {
    title: string;
    id: number;
    releaseDate: string;
    overview: string;
    posterPath: string | null;
    constructor(
        title: string,
        id: number,
        release_date: string,
        overview: string,
        poster_path: string | null
    ) {
        this.title = title;
        this.id = id;
        this.releaseDate = release_date;
        this.overview = overview;
        this.posterPath = poster_path;
    }
}

export default Movie;
